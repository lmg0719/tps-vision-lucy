#Gimenez Lucy de la Merced, Legajo:69899
#TP11: Alineación de imágenes usando SIFT -2022

import numpy as np
import cv2

blue = (255, 0, 0); green = (0, 255, 0); red = (0, 0, 255)

MIN_MATCH_COUNT = 10

#a)Leemos dos imágenes con diferentes vistas del mismo objeto
img1 = cv2.imread('imagen1.jpeg') 	#Leemos img1
img2 = cv2.imread('imagen2.jpeg') 	#Leemos img2
img1_copy = img1.copy()
img2_copy = img2.copy()

dscr = cv2.xfeatures2d.SIFT_create(100) 	 #Inicializamos el detector y el descriptor

#b)Computamos los puntos de interés y descriptores en ambas imágenes
kp1, des1 = dscr.detectAndCompute(img1, None)	#Encontramos los puntos clave y los descriptores con SIFT en img1
kp2, des2 = dscr.detectAndCompute(img2, None)	#Encontramos los puntos clave y los descriptores con SIFT en img2

# Gráfica de puntos clave
cv2.drawKeypoints(img1,kp1,img1_copy,red)
cv2.drawKeypoints(img1,kp2,img2_copy,blue)
img_concatenate = np.concatenate((img1_copy, img2_copy), axis=0)	#Concatenamos verticalmente
cv2.imshow('Puntos clave de las imagenes: Presionar una tecla para continuar', img_concatenate)
cv2.imwrite('img_Puntos_clave.jpeg',img_concatenate)
cv2.waitKey(0)

#c) Establecemos los matches
matcher = cv2.BFMatcher(cv2.NORM_L2)
matches = matcher.knnMatch(des1, des2, k=2)

#d)Eliminamos matches usando criterio de Lowe
#Guardamos los buenos matches usando el test de razón de Lowe
good = []
all = []
for m, n in matches:
	all.append(m)
	if m.distance < 0.7*n.distance:
		good.append(m)

if(len(good)>MIN_MATCH_COUNT):
	scr_pts = np.float32([kp1[m.queryIdx].pt for m in good]).reshape(-1, 1, 2)
	dst_pts = np.float32([kp2[m.trainIdx].pt for m in good]).reshape(-1, 1, 2)
	
#e) Computamos una homografía entre un conjunto de puntos y el otro	
	H,mask = cv2.findHomography(dst_pts, scr_pts, cv2.RANSAC, 5.0) #Computamos la homografía con RANSAC

wimg2 = cv2.warpPerspective(img2, H, img2.shape[:2][::-1]) #Aplicamos la transformación perspectiva H a img2

# Gráfica de matches pre Lowe
img_matches = cv2.drawMatches(img1, kp1, img2, kp2, all, None)	
cv2.imshow('Matches pre Lowe: Presionar una tecla para continuar', img_matches)
cv2.imwrite('Matches_pre_lowe.jpeg',img_matches)
cv2.waitKey(0)

# Gráfica de matches post Lowe
img_matches = cv2.drawMatches(img1, kp1, img2, kp2, good, None)	
cv2.imshow('Matches post Lowe: Presionar una tecla para continuar', img_matches)
cv2.imwrite('Matches_post_lowe.jpeg',img_matches)
cv2.waitKey(0)

#f) Aplicamos la homografía sobre una de las imágenes y guardarla en otra (mezclarla con un alpha de 50 %)
#Mezclamos ambas imágenes y mostramos la imagen de salida
alpha = 0.5
blend = np.array( wimg2*alpha + img1*(1-alpha), dtype=np.uint8)
cv2.imshow('Imagen de salida: Presionar una tecla para continuar', blend)
cv2.imwrite('img_salida.jpeg',blend)
cv2.waitKey(0)

cv2.destroyAllWindows
