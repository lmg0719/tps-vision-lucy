#Gimenez Lucy de la Merced, Legajo: 69899
#TP8: Rectificando imágenes- Visión por computadora -2022
#A)Crear una función que compute la homografía entre los 4 pares de puntos correspondientes.
#B)Usando como base el programa anterior, escriba un programa que con la letra “h” permita seleccionar con el mouse 4 puntos no colineales en una imagen y transforme (rectifique) la selección en una nueva imagen rectangular.
#Ayuda:
#cv2.getPerspectiveTransform
#cv2.warpPerspective

import cv2 
import numpy as np
import math

blue = (255, 0, 0); green = (0, 255, 0); red = (0, 0, 255);
drawing = False
Coord = 0, 0
Coord_1 = 0, 0
Coord_2 = 0, 0
Coord_3 = 0, 0
Coord_4 = 0, 0
aux = 0

def mouse (evento, x, y, flags, param ) :
    global Coord_1, Coord_2, Coord_3, Coord_4, drawing, img_respaldo, aux
    if evento == cv2.EVENT_LBUTTONDOWN:
        drawing = True
        Coord = x, y
        cv2.circle(img_respaldo, Coord, radius=2, color=(0, 0, 0), thickness=2)
        if aux == 0:
            Coord_1 = Coord
            aux += 1
        elif aux == 1:
            Coord_2 = Coord
            aux += 1
        elif aux == 2:
            Coord_3 = Coord
            aux += 1
        elif aux == 3:
            Coord_4 = Coord

def homografia (punto_1, punto_2, imagen):
    al, an = imagen.shape[:2]
    Matriz = cv2.getPerspectiveTransform(punto_1, punto_2)
    img_salida = cv2.warpPerspective(imagen, Matriz, (al, an))
    return img_salida

img_original = cv2.imread ('Imagen.png', 1)
img = cv2.resize(img_original, (350, 450)) #(350, 450))
img_respaldo = img.copy()                       # Imagen utilizada para resetear el area del rectangulo
img_homografica = img.copy()                    # Imagen para guardar la homografia
cv2.namedWindow ('Imagen original.')
cv2.setMouseCallback ('Imagen original.', mouse)
while (1) :
    cv2.imshow ('Imagen original.', img_respaldo) #muestra la imagen original, con la tecla 'a' selecciono los 4 ptos no colineales
    k = cv2.waitKey (1) & 0xFF
    if k == ord("r"):				#resetear puntos seleccionados, para poder volver a seleccionar los 4 ptos no colineales
        img_respaldo = img.copy()
    elif k == ord("h"):			#transforma (rectifica) la selección en una nueva imagen rectangular
        #al, an = img.shape[:2]
        al, an = (300, 200)         # Tamano del cuadro 
        punto_1 = np.float32([[Coord_1], [Coord_2], [Coord_3], [Coord_4]]) #orden de ptos: empieza arriba de izq a der, luego abajo, de izq a der
        punto_2 = np.float32([[0, 0], [an, 0], [0, al], [an, al]])
        img_salida = homografia(punto_1, punto_2, img)
        img_salida = img_salida[0:al, 0:an]
        cv2.imshow('Imagen con homografia', img_salida)	#muestra en pantalla la imagen con homografia 
        cv2.imwrite('Imagen rectificada.png', img_salida)     #guarda la imagen rectificada en la carpeta donde se encuentra este programa .py
        Coord_1 = Coord_2 = Coord_3 = Coord_4 = 0, 0
        aux = 0
    elif k == ord('q'):				 	#q= para salir del programa
        break
cv2.destroyAllWindows()


