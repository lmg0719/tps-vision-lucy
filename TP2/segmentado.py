#Gimenez Lucy de la Merced, Legajo:69899
#TP2: Segmentado -Visión por computadora-2022
#Escribir un programa en python que lea una imagen y realice un umbralizado binario, guardando el resultado en otra imagen.

import cv2

img = cv2.imread( 'hoja.png' , 0) #lee una imagen en escala de grises

#Para resolverlo podemos usar dos for anidados

row=len(img) #len() devuelve la cantidad de elementos (o longitud) de la matriz
col=len(img)
for i in range(row):     #Se recorre la matriz
    for j in range(col):
        if img[i][j]<220:
            img[i][j] = 0       #Negro
        else:
            img[i][j] = 255     #Blanco

img_nueva=cv2.imwrite( 'resultado.png' , img) #crea y guarda la imagen modificada

cv2.imshow ( ' resultado.png' , img)	#Muestra la imagen modificada en pantalla 
cv2.waitKey(0)				#Espera durante retardo ms a que se presione una tecla
cv2.destroyAllWindows()		#cierra/destruye todas las ventanas, una vez presionada una tecla


