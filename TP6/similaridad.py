#Gimenez Lucy de la Merced, Legajo: 69899
#TP6: Transformación de similaridad- Visión por computadora -2022
#A)Agregar a la función anterior un parámetro que permita aplicar un escalado a la porción rectangular de imagen. Parámetros: angle,tx,ty,s
#B) Luego, usando como base el programa anterior, escribir un programa que permita seleccionar una porción rectangular de una imagen

import cv2 
import numpy as np
import math

blue = (255, 0, 0); green = (0, 255, 0); red = (0, 0, 255);
drawing = False						#drawing=True si se hace click con el mouse
mode = True							#mode=True indica rectángulo, con 'm' cambia a línea
Coord_inicial = 0, 0						
Coord_final = 0, 0						#Se guarda la posición donde se soltó el click para recortar la imagen

def rectangulo (event, x, y, flags, param ) :
    global Coord_inicial , drawing , mode, img_respaldo, img, Coord_final
    if event == cv2.EVENT_LBUTTONDOWN:
        drawing = True
        Coord_inicial = x , y
    elif event == cv2.EVENT_MOUSEMOVE:
        if drawing is True :
            img_respaldo = img.copy ()
            if mode is True:
                cv2.rectangle (img_respaldo, Coord_inicial, (x, y), blue , 2)
                Coord_final = x, y
    elif event == cv2.EVENT_LBUTTONUP:
        drawing = False

def recorte ():
    global Coord_inicial, Coord_final, img, img_respaldo
    x1, y1 = Coord_inicial
    x2, y2 = Coord_final
    imagen2 = img[y1:y2, x1:x2]
    img_respaldo = imagen2.copy()
    img = imagen2.copy()
    return imagen2

def traslacion (imagen, x, y):
    (h, w) = (imagen.shape[0], imagen.shape[1])
    M = np.float32([[1, 0, x],
                   [0, 1, y]])
    imagen1 = cv2.warpAffine(imagen, M, (w, h))
    return imagen1

def rotacion (imagen, angulo, centro, escala=1):
    (h, w) = imagen.shape[:2]
    M = cv2.getRotationMatrix2D(centro, angulo, escala)
    imagen2 = cv2.warpAffine(imagen, M, (w, h))
    return imagen2

def rot_tras (imagen1, x, y, angulo, ancho, alto):
    escala = 1
    img_tras = traslacion(imagen1, x, y)
    cv2.imshow ('Imagen trasladada.', img_tras)
    img_rotada = rotacion (img_tras, angulo, centro=(ancho/2, alto/2))
    cv2.imshow('Imagen rotada.', img_rotada)
    return img_rotada

def similaridad (imagen2, x, y, angulo, ancho, alto, escala):
    M = np.float32([[escala * math.cos(angulo * math.pi / 180), escala * math.sin(angulo * math.pi / 180), x],
                    [-escala * math.sin(angulo * math.pi / 180), escala * math.cos(angulo * math.pi / 180), y]])
    img_escalada = cv2.warpAffine(imagen2, M, (ancho, alto))
    cv2.imshow('Imagen escalada.', img_escalada)
    return img_escalada

img = cv2.imread ('paisaje.jpg', 1)
img_respaldo = img.copy ()      # Imagen utilizada para resetear el area del rectangulo
img_respaldo2 = img.copy ()     # Imagen para volver atras el recorte
img_rec = img.copy ()           # Imagen para guardar el recortar
img_euclidiana = img.copy ()    # Imagen para guardar la rotacion y traslacion
img_similaridad = img.copy ()
cv2.namedWindow ('Imagen original.')
cv2.setMouseCallback ('Imagen original.', rectangulo)
while (1) :
    cv2.imshow ('Imagen original.', img_respaldo)
    k = cv2.waitKey (1) & 0xFF
    if k == ord("r"):				#r= para resetear el cuadrado elegido y volver a seleccionarlo
        img_respaldo = img_respaldo2.copy()
        img = img_respaldo2.copy ()
    elif k == ord('g'):			#g= para guardar la imagen modificada
        img_rec = recorte()
        cv2.imwrite('Imagen recortada.jpg', img_rec)
    elif k == ord("e"):			#e= para realizar la transformacion euclidiana a la imagen recortada
        imagen1 = recorte()
        alto, ancho = imagen1.shape[0:2]
        print('Elija cuantos pixels se movera hacia la derecha (entre 0 y {})'.format(ancho)) #traslacion1
        x = input()									#los valores se ingresan por teclado
        print('Elija cuantos pixels se movera hacia abajo (entre 0 y {})'.format(alto)) #traslacion2
        y = input()
        print('Ingresar el angulo de rotacion para la imagen?')	#angulo de rotacion
        angulo = int(input())
        img_euclidiana = rot_tras(imagen1, x, y, angulo, ancho, alto)
        cv2.imwrite('Imagen euclidiana.jpg', img_euclidiana)
        cv2.imshow ('Imagen euclidiana.jpg', img_euclidiana)
    elif k == ord("s"):		#'s': para aplicar una transformación de similaridad a la porción de imagen seleccionada y la 						guarde como una nueva imagen.
        imagen2 = recorte()
        alto, ancho = imagen2.shape[0:2]
        print('Ingresar numero de pixels que se movera hacia la derecha (entre 0 y {})'.format(ancho))
        x = input()
        print('Ingresar numero de pixels que se movera hacia abajo (entre 0 y {})'.format(alto))
        y = input()
        print('Ingresar grados para rotar la imagen')
        angulo = int(input())
        print('Ingresar la escala para la transformacion') # se agregó el escalado como nuevo parámetro 
							     # para la porción rectangular de imagen.
        escala = float(input())
        img_similaridad = similaridad(imagen2, x, y, angulo, ancho, alto, escala)
        cv2.imwrite('Imagen de similaridad.jpg', img_similaridad)
        cv2.imshow('Imagen de similaridad.jpg', img_similaridad)
    elif k == ord('q'):	#q=para salir del programa
        break

cv2.destroyAllWindows()

