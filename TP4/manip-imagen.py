#Gimenez Lucy de la Merced, Legajo: 69899
#TP4: Manipulación de imagenes- Visión por computadora -2022
#Usando como base el programa dado, escribir un programa que permita seleccionar una porción rectangular de una imagen

import cv2 
import numpy as np

blue = (255, 0, 100); green = (0, 255, 100); red = (0, 0, 255); #Para cambiar el color del rectángulo
drawing = False # true si el botón está presionado
mode = True
xybutton_down = -1,-1
Coord_final = 0, 0

def dibuja (event, x, y, flags, param ) :
    global xybutton_down , drawing , mode, img_aux, img, Coord_final
    if event == cv2.EVENT_LBUTTONDOWN:	#EVENT_LBUTTONDOWN: se presionó el botón izquierdo
        drawing = True
        xybutton_down = x , y
    elif event == cv2.EVENT_MOUSEMOVE: #EVENT_MOUSEMOVE: el puntero del mouse se movió
        if drawing is True :
            img_aux = img.copy ()
            if mode is True:
                cv2.rectangle (img_aux, xybutton_down, (x, y), blue , 2)
                Coord_final = x, y
    elif event == cv2.EVENT_LBUTTONUP: #EVENT_LBUTTONUP: se soltó el botón izquierdo
        drawing = False

img = cv2.imread ('Imagen_original.jpg', 1) 
img_aux = img.copy ()	#.copy se utiliza para clonar una imagen. Copia la imagen a otra imagen objeto
img_aux2 = img.copy ()	#Es útil cuando necesitamos copiar la imagen pero también retener la original
#Creamos una ventana y capturamos los eventos del mouse en esa ventana
cv2.namedWindow ('Imagen')		
cv2.setMouseCallback ('Imagen', dibuja)

while (1) :
    #Usamos la ventana creada para mostrar la imagen
    cv2.imshow ('Imagen', img_aux)
    k = cv2.waitKey (1) & 0xFF
    if k == ord('g'):			#'g': para guardar la porción de imagen seleccionada con la nueva imagen
        x1, y1 = xybutton_down
        x2, y2 = Coord_final
        img_rec = img[y1:y2, x1:x2] 
        img_aux = img_rec.copy ()
        img = img_rec.copy ()
        cv2.imwrite ('Imagen_recortada.jpg', img_rec)
    elif k == ord("r") :		#'r' para restaurar la imagen original y permitir realizar una nueva selección
        img_aux = img_aux2.copy()
        img = img_aux2.copy ()
    elif k == ord('q') :		#'q' para finalizar
        break
cv2.destroyAllWindows ( )

