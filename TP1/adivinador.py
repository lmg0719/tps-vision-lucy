#Gimenez Lucy de la Merced, Legajo:69899
#TP1: Adivinador - Visión por computadora -2022

print('\nJUEGO ADIVINADOR!')
print('\nEl juego consiste en adivinar un numero entre 0 y 100')

def adivina(n_intentos):                      #unción adivina que permita adivinar un número secreto generado en forma aleatoria
					       #La función adivina recibe como parámetro la cantidad de intentos permitidos
	import random
	nro_a_adivinar=random.randint(0,100) #Crea un nro random entre 0 y 100 y lo almacena en la 
					      #variable nro_a_adivinar
	N = True
	contador=0
	while N:   
            nro_ingresado = int(input('Ingrese un entero entre 0 y 100: '))
            contador = contador + 1
            if nro_ingresado == nro_a_adivinar:
                print('Felicidades, adivinaste el valor secreto en el intento nro', contador) 
	       #sale del bucle while
                N = False 
            elif nro_ingresado > nro_a_adivinar:
                print('\nIncorrecto, el valor secreto es MENOR al valor ingresado')
            else: 
                print('\nIncorrecto, el valor secreto es MAYOR al valor ingresado')	
            if nro_intentos == contador:
                print('\nLa cantidad de intentos fue superada')
                break
            else:
                print('\nLe quedan ', n_intentos-contador, 'intentos')
	
	print('\nFIN DEL JUEGO')    

nro_intentos = int (input("\nIngrese la cantidad de intentos: "))

adivina(nro_intentos) 		 

		     
		     
		     
		     
		     
		     
		     
		     
		
		
		
		
		
		
		
		
		
		
			
			
			
			
