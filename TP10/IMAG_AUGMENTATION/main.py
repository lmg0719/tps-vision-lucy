#Gimenez Lucy de la Merced, Legajo: 69899
#TP10: Aruco- Visión por computadora -2022
#FUNCIONAMIENTO: Se utiliza el celular apuntando hacia la camara de la notebook, con distintos codigos arucos de 4x4_50
#Se utiliza una lista de imagenes, que pueden ser modificadas, se encuentran en la carpeta images, estas mismas se superponen en los arucos en 2D 
#Se utiliza la cámara de la notebook para grabar y guardar la captura de video (el video se encuentra en IMAG_AUGMENTATION) 
#Al presionar 'q' sale del programa y guarda la captura de video
import cv2 as cv
from cv2 import aruco	#debieron ser instaladas varias librerias para lograr la deteccion de 
                        #los arucos
import numpy as np
import os

def image_augmentation(frame, src_image, dst_points):
    src_h, src_w = src_image.shape[:2]
    frame_h, frame_w = frame.shape[:2]
    mask = np.zeros((frame_h, frame_w), dtype=np.uint8)
    src_points = np.array([[0, 0], [src_w, 0], [src_w, src_h], [0, src_w]])
    H, _ = cv.findHomography(srcPoints=src_points, dstPoints=dst_points)
    warp_image = cv.warpPerspective(src_image, H, (frame_w, frame_h))
    cv.imshow("warp image", warp_image)
    cv.fillConvexPoly(mask, dst_points, 255)
    results = cv.bitwise_and(warp_image, warp_image, frame, mask=mask)

def read_images(dir_path):
    img_list = []
    files = os.listdir(dir_path)
    for file in files:
        img_path = os.path.join(dir_path, file)
        image = cv.imread(img_path)
        img_list.append(image)
    return img_list


marker_dict = aruco.Dictionary_get(aruco.DICT_4X4_50)

param_markers = aruco.DetectorParameters_create()

images_list = read_images("../images/augmentation") #lee las imagenes que serán posicionadas sobre los arucos respectivamente

cap = cv.VideoCapture(0) #para realizar un video streaming con la camara de la notebook
salida = cv.VideoWriter('videoSalida.avi',cv.VideoWriter_fourcc(*'XVID'),20.0,(640,480)) ####Graba y guarda el video hasta presionar la 										tecla 'q' para salir del programa
while True:		#mientras sea True significa que hay lectura de la camara 	
    ret, frame = cap.read()
    if not ret:	
        break
    gray_frame = cv.cvtColor(frame, cv.COLOR_BGR2GRAY)
    marker_corners, marker_IDs, reject = aruco.detectMarkers(	#en esta seccion se realiza la deteccion de arucos
        gray_frame, marker_dict, parameters=param_markers	       #para facilitar la deteccion de bordes el pdf tiene un fondo color blanco
    )
    if marker_corners:
        for ids, corners in zip(marker_IDs, marker_corners):

            corners = corners.reshape(4, 2)
            corners = corners.astype(int)
            if ids[0] <= 12:						   #A cada codigo aruco se le asigna una imagen (que será superpuesta en el 											mismo)
                image_augmentation(frame, images_list[ids[0]], corners)
            else:
                image_augmentation(frame, images_list[0], corners)
            # print(ids, "  ", corners)
    cv.imshow("frame", frame)
    salida.write(frame) ######### Este tipo de linea de codigo representa el guardado del video 
    key = cv.waitKey(1)
    if key == ord("q"):	#q=para salir del programa
        break
cap.release()
salida.write(frame) #######
cv.destroyAllWindows()




