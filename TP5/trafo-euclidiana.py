#Gimenez Lucy de la Merced, Legajo: 69899
#TP5: Rotación + Traslación (o Transformación Euclidiana)- Visión por computadora -2022
#A)Crear una función que aplique una transformación euclidiana, recibiendo los siguientes parámetros: angle,tx,ty.
#B)Usando como base el programa anterior, escribir un programa que permita seleccionar una porción rectangular de una imagen

import cv2 
import numpy as np

blue = (255, 0, 100); green = (0, 255, 100); red = (0, 0, 255);
drawing = False
mode = True
xybutton_down = -1,-1
Coord_final = 0, 0

def dibuja (event, x, y, flags, param ) : #misma funcion para dibujar rectangulo utilizada en TP4
    global xybutton_down , drawing , mode, img_aux, img, Coord_final
    if event == cv2.EVENT_LBUTTONDOWN:	#EVENT_LBUTTONDOWN: se presionó el botón izquierdo
        drawing = True
        xybutton_down = x , y
    elif event == cv2.EVENT_MOUSEMOVE:   #EVENT_MOUSEMOVE: el puntero del mouse se movió
        if drawing is True :
            img_aux = img.copy ()
            if mode is True:
                cv2.rectangle (img_aux, xybutton_down, (x, y), blue , 2)
                Coord_final = x, y
    elif event == cv2.EVENT_LBUTTONUP:		#EVENT_LBUTTONUP: se soltó el botón izquierdo
        drawing = False

def recorte ():
    global xybutton_down, Coord_final, img, img_aux
    x1, y1 = xybutton_down				
    x2, y2 = Coord_final
    imagen2 = img[y1:y2, x1:x2]
    img_aux = imagen2.copy()
    img = imagen2.copy()
    return imagen2

def translate (imagen, x, y):				#Código para la traslación
    (h, w) = (imagen.shape[0], imagen.shape[1])	

    M = np.float32([[1, 0, x],
                   [0, 1, y]])

    imagen1 = cv2.warpAffine(imagen, M, (w, h))	#usamos la func: dst = cv2.warpAffine(src, M, dsize[, dst])
    #cv2.imshow('Imagen trasladada.', imagen1)
    return imagen1

def rotate (imagen, angle, center, scale=1):	#Código para la rotación
    (h, w) = imagen.shape[:2]

    M = cv2.getRotationMatrix2D(center, angle, scale)
    imagen2 = cv2.warpAffine(imagen, M, (w, h))	#usamos el método M =cv2.getRotationMatrix2D(center, angle, scale) 
    #cv2.imshow('Imagen rotada.', imagen2)		#para calcular la matriz de rotación
    return imagen2

def rot_tras (imagen1, x, y, angle, ancho, alto):
    img_tras = translate(imagen1, x, y)
    
    img_rot = rotate (img_tras, angle, center=(ancho/2, alto/2))
   
    return img_rot

img = cv2.imread ('Imagen.jpg', 1)
img_aux = img.copy ()      # img_aux= para resetear el área del rectángulo
img_aux2 = img.copy ()     # img_aux2= para volver atrás el recorte
img_rec = img.copy ()      # img_rec= para guardar el recorte
img_euclidiana =img.copy() # img_euclidiana= para guardar la rotación y traslación
#Creamos una ventana y capturamos los eventos del mouse en esa ventana
cv2.namedWindow ('Imagen original.')
cv2.setMouseCallback ('Imagen original.', dibuja)

while (1) :
    cv2.imshow ('Imagen original.', img_aux)
    k = cv2.waitKey (1) & 0xFF
    if k == ord("r"):		#'r' para restaurar la imagen original y permitir realizar una nueva selección
        img_aux = img_aux2.copy()
        img = img_aux2.copy ()
    elif k == ord('g'):	#'g': para guardar la porción de imagen seleccionada con la nueva imagen
        img_rec = recorte()
        cv2.imwrite('Imagen-recortada.jpg', img_rec)
    elif k == ord("e"):		#'e': para aplicar una transformación euclidiana a la porción de imagen seleccionada y la guarde 						como una nueva imagen.
        imagen1 = recorte()
        alto, ancho = imagen1.shape[0:2]
        print('Ingresar n pixels para mover la imagen hacia la derecha (entre 0 y {})'.format(ancho))
        x = input()
        print('Ingresar n pixels para mover la imagen hacia abajo (entre 0 y {})'.format(alto))
        y = input()
        print('Ingresar nro en grados que se quiere rotar la imagen')
        angle = int(input())

        img_euclidiana = rot_tras(imagen1, x, y, angle, ancho, alto)
        cv2.imwrite('Imagen-euclidiana.jpg', img_euclidiana)
        cv2.imshow ('Imagen euclidiana.', img_euclidiana)

        print('Ingresar la letra q para finalizar')
    elif k == ord('q'):	#q=para finalizar
        break
cv2.destroyAllWindows()

