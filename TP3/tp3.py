#Gimenez Lucy de la Merced, Legajo: 69899
#TP3: Visión por computadora -2022

import sys                                                  
import cv2                                                   
						    #comando para ejecución: python3 tp3.py nombre, sino mostrará un mensaje de error 
if(len(sys.argv)>1):                              #Verificamos que se llame correctamente al video
    filename = sys.argv [1]                      #Si se cumple la condición, se trae el archivo y se asigna el argumento pasado a filename 
					 #Guarda en filename: el primer argumento es el nombre del archivo y el segundo argumento es el video)
else:                                                        #SINO, 
    print ('Pass a filename as first argument')      #Se muestra en pantalla un mensaje solicitando al usuario que 'pase el 
							#nombre del archivo como primer argumento'
    sys.exit (0) 
							#Si el filename es correcto:
cap = cv2.VideoCapture ('/dev/video0')                       #Captura de un video (se abre el video) desde el dispositivo '/dev/video0'
fourcc = cv2.VideoWriter_fourcc ('X','V','I','D')            #Codigo de 4bytes usado para el codec 
ret, frame = cap.read()                                      #Lectura de una imagen para almacenar el tamanio

A = int(cap.get(cv2.CAP_PROP_FRAME_HEIGHT))                  #Leemos la altura "A"
B = int(cap.get(cv2.CAP_PROP_FRAME_WIDTH))                   #Leemos el ancho "B" 
framesize=(B,A)                               		#Designa el framesize
fps = cap.get(cv2.CAP_PROP_FPS)                              #Se obtienen las imágenes por segundo
delay = int(1000/fps)                                        #Delay en milisegundos entre imagen e imagen = 1/(fps/1000) 
out = cv2.VideoWriter ('output1.avi',fourcc, fps,framesize,0)    #Creamos el archivo de salida, con el nombre 'output1.avi'


while (cap.isOpened()):                                      #Verificamos que se abra correctamente el video y capturamos frame a frame
    ret,frame = cap.read()                                   #Se realiza la lectura y se lo almacena en la tupla
    if ret is True:
        gray = cv2.cvtColor (frame,cv2.COLOR_BGR2GRAY)       #Convertimos a escala de grises 
        out.write (gray)                                     
        cv2.imshow ('Image',frame)                           #Mostramos la imagen obtenida
        if cv2.waitKey(delay)&0xFF == ord('q'):              #El video se grabará mientras no se presione la tecla 'q',  y con un delay segun el fps, luego sale
            break
    else:
         break                                               #Si no se abre correctamente la imagen, sale

#Cierra la camara, video y ventanas   
cap.release ()                                              
out.release ()
cv2.destroyAllWindows()
